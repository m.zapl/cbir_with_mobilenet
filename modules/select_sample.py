import random
import pathlib
import shutil
import logging
import argparse

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def main():
    """
    Function to sample images from folder to train MobileNet with them.

    Args:
        -s, --source-path: source folder path with all images
        -t, --target-path: folder path where to store sample of images
        -n, --n-sam: number of samples. Default 20_000
    """
    parser = argparse.ArgumentParser(description='Get sample of images from folder')
    parser.add_argument("-s", '--source-path', nargs="?", type=str, help='source folder path with all images')
    parser.add_argument("-t", '--target-path', nargs="?", type=str, help='folder path where to store sample of images')
    parser.add_argument("-n", '--n-sam', type=int, help='number of samples', default=20_000)

    args = parser.parse_args()

    path = pathlib.Path(args.source_path)
    files = path.glob("*")

    names = random.sample(list(files), args.n_sam)

    destination = pathlib.Path(args.target_path)
    destination.mkdir(exist_ok=True)

    for file in names:
        shutil.copy(file, destination)

    logger.info(f"Sample written to {str(destination)}")


if __name__ == "__main__":
    main()
