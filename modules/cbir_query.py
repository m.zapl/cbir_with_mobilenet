import logging
from pathlib import Path
from typing import List, Union
import h5py  # It lets you store huge amounts of numerical data - https://www.h5py.org/
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg as LA
from modules.feature_extraction import feature_extraction
from modules.mobilenet import MobileNet

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def draw_images(
    query_path: Path,
    imlist: List,
    db_path: Path = Path("database_20k")
):
    """
    This function draw query image and the most similar images

    Args:
        query_path (Path): path to query image
        imlist (List): list of names of most similar images
        db_path (Path, optional): Path do database with images.
            Defaults to Path("database_20k").
    """

    image = mpimg.imread(query_path)
    plt.imshow(image)
    plt.show()
    # show top #maxres retrieved result one by one
    for i, im in enumerate(imlist):
        image = mpimg.imread(db_path / im)
        plt.title("search output %d" % (i + 1))
        plt.imshow(image)
        plt.show()


def show_similar_imgs(
    query_path: Union[str, Path],
    mobilenet: MobileNet,
    db_path: Union[str, Path] = "database_20k",
    feature_path="features/index.h5",
    maxres: int = 3,
):
    """
    This function print query image and the most similar images

    Args:
        query_path (Union[str, Path]): path to query image
        mobilenet (MobileNet): mobilenet class
        db_path (Union[str, Path], optional): Path do database with images.
            Defaults to "database_20k".
        feature_path (str, optional): Path to feature vectors of images.
            Defaults to "features/index.h5".
        maxres (int, optional): How many similar images to draw. Defaults to 3.
    """

    assert Path(query_path).exists(), "The input file does not exist."
    logger.info("Image found, processing...")

    feature_path = Path(feature_path)
    db_path = Path(db_path)

    if not feature_path.exists():
        feature_extraction(
            output_path=feature_path,
            mobilenet=mobilenet,
            db_path=db_path
            )

    with h5py.File(feature_path, "r") as h5f:
        feats = h5f["dataset_1"][:]
        imgNames = h5f["dataset_2"].asstr()[:]

    query_feat = mobilenet.extract_feat(query_path)
    query_norm_feat = query_feat[0] / LA.norm(query_feat[0])  # get cosine similarity

    scores = np.dot(np.array(query_norm_feat), np.squeeze(feats).T)
    rank_ID = np.argsort(scores)[::-1]

    imlist = [imgNames[index] for i, index in enumerate(rank_ID[0:maxres])]

    draw_images(query_path, imlist, db_path)
