import logging
from pathlib import Path
from modules.mobilenet import MobileNet
import h5py  # It lets you store huge amounts of numerical data - https://www.h5py.org/
import numpy as np
from numpy import linalg as LA

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def feature_extraction(
    output_path: Path,
    mobilenet: MobileNet,
    db_path: Path = Path("database_20k")
):
    """
    It extracts feature vector of images in db_path to which
    we will compare our query pictures.

    Args:
        output_path (Path): path where to save feature vector
        mobilenet (MobileNet): class mobilenet
        db_path (Path, optional): Path do database with images.
            Default: database_20k. Defaults to Path("database_20k").
    """

    output_path.parent.mkdir(exist_ok=True)
    img_list = list(db_path.glob("*.jpg"))

    feats_norm = []
    names = []

    logger.info("Preparing feature vectors... (this may take up to 30 minutes)")

    for i, img_path in enumerate(img_list):
        if i % 1000 == 0:
            logger.info(f"{i} images out of {len(img_list)} done")
        feat = mobilenet.extract_feat(str(img_path))
        norm_feat = feat[0] / LA.norm(feat[0])

        feats_norm.append(norm_feat)
        names.append(img_path.name)

    feats_np = np.array(feats_norm)

    with h5py.File(output_path, "w") as h5f:
        h5f.create_dataset("dataset_1", data=feats_np)
        h5f.create_dataset("dataset_2", data=names)

    logger.info(f"Feature vectors written to {str(output_path)}")
