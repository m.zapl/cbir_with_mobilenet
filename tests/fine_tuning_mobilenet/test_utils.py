import pytest
import numpy as np
from pathlib import Path
from datetime import datetime
from PIL import Image
from fine_tuning_mobilenet.modules.utils import prep_img


def create_moc_img() -> Path:
    """
    Create and save mock img

    Returns:
        Path: where the img is saved
    """
    input_image = np.array([[1, 1], [1, 1]])
    img = Image.fromarray(input_image, 'RGB')

    # path to save tmp image
    t = datetime.now().strftime("%Y%m%d_%H%M%S")
    dir = Path('./tests/fine_tuning_mobilenet/tmp/moc_test_images/')
    img_path = dir / 'moc_img_{}.jpeg'.format(t)

    # create dir for tmp image if not exists
    dir.mkdir(parents=True, exist_ok=True)

    # save img
    img.save(img_path)

    return img_path


@pytest.mark.parametrize(
    'test_input, expected_output',
    [
        (False, np.full((1, 224, 224, 3), -1.)),
        (True, np.full((224, 224, 3), -1.))
    ],
)
def test_prep_img(test_input, expected_output):

    img_path = create_moc_img()

    assert np.array_equal(
        prep_img(img_path, test_input),
        expected_output
        )

    # delete tmp img
    Path(img_path).unlink()
